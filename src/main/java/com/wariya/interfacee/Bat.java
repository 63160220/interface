/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wariya.interfacee;

/**
 *
 * @author os
 */
public class Bat extends Poultry{
    private String nickname;
    public Bat(String nickname) {
        super();
        this.nickname = nickname;
    }

    @Override
    public void eat() {
        
    }

    @Override
    public void walk() {
        
    }

    @Override
    public void speak() {
        
    }

    @Override
    public void sleep() {
        
    }

    @Override
    public void fly() {
        System.out.println("Bat: "+nickname+" fly");
    }
    
}

